rm -rf /opt/ANDRAX/apktool

mkdir /opt/ANDRAX/apktool

wget https://bitbucket.org/iBotPeaches/apktool/downloads/apktool_2.9.3.jar

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Download APKTOOL... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf apktool_2.9.3.jar /opt/ANDRAX/apktool

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
